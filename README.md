- Array method
    * `reverse`
    * `shift` remove first item
    * `unshift`
    * `pop`
    * `push`
    * `splice`
    * `slice`
    * `indexOf(search, index)`
    * `join`


- DOM accessing
    * `getElementById`
    * `getElementByClassName`
    * `getElementByTagName`
    * `querySelector`
    * `querySelectorAll`


- Access and change elements
    * `innerHTML`
    * `outerHTML`
    * `id`
    * `className`
    * `classList`


- Change classes
    * `document.querySelector('.masthead').classList[1]`
    * `document.querySelector('.masthead').classList.add('new-class')`
    * `document.querySelector('.masthead').classList.remove('clear')`
    * `document.querySelector('.new-class').classList.toggle('masthead')`


- Access and change attributes
    * `hasAttribute`
    * `getAttribute`
    * `setAttribute`


- Apply inline CSS to an element
    * `document.querySelector(".cta a").style`
    * `document.querySelector(".cta a").style.color = "green"`
    * `document.querySelector(".cta a").cssText = "padding: 1em;"`
    * `document.querySelector(".cta a").setAttribute("style", "padding: 1em;")`


- Map methods: `keys`, `values`, `entries`

- Symbols
    
        :::javascript
        const id = Symbol();
        const courseInfo = {
          title: "ES6",
          topics: ["babel", "syntax", "functions", "classes"],
          id: "js-course"
        };
		    courseInfo[id] = 41284;


- Iterators

        :::javascript
        var title = 'ES6';
        var iterateIt = title[Symbol.iterator]()
        console.log(iterateIt.next())
    

- Custom iterator

        :::javascript
        function tableReady(arr) {
          var nextIndex = 0
          return {
            next() {
              if(nextIndex < arr.length) {
                return {value: arr.shift(), done: false}
              } else {
                return {done: true}
              }
            }
          }
        }
        var waitingList = ['Sarah', 'Heather', 'Anna', 'Meagan']
        var iterateList = tableReady(waitingList)


- Async and fetch

        :::javascript
        var response = await fetch("https://api.github.com/users/dat/followers")
				var json = await response.json()
				var followerList = json.map(user => user.login)