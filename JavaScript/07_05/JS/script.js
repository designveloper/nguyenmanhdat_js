const CTA = document.querySelector(".cta a");
const ALERT = document.querySelector("#booking-alert");

CTA.classList.remove("hide");
ALERT.classList.add("hide");

function reveal(node) {
    node.innerHTML == "Book Now!" ? CTA.innerHTML = "Oooops!" : "Book Now!"
    ALERT.classList.toggle("hide");
}

CTA.addEventListener('click', function() { reveal(this) }, false);
CTA.addEventListener('click', function(){console.log("The button was clicked!")}, false);
