function Course(title, instructor, level, published, views) {
    this.title = title
    this.instructor = instructor
    this.level = level
    this.published = published
    this.views = views
    this.updateView = function() {
        return ++this.views
    }
}

var course1 = new Course("JavaScript Essential Training", "Morter", 1, true, 0)